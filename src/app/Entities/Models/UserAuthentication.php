<?php

namespace App\Entities\Models;

use App\Entities\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UserAuthentication
 * @package App\Entities\Models
 */
class UserAuthentication extends Model
{
    use UuidTrait;

    /**
     * @var string[]
     */
    protected $fillable = [
        'email',
        'password',
    ];

    /**
     * @var string[]
     */
    protected $hidden = [
        'password',
    ];

    /**
     * @param string $value
     */
    public function setPasswordAttribute(string $value = ''): void
    {
        $this->attributes['password'] = bcrypt($value);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
