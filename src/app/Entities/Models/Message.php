<?php

namespace App\Entities\Models;

use Baopham\DynamoDb\DynamoDbModel as Model;

/**
 * Class Message
 * @package App\Entities\Models
 */
class Message extends Model
{
    /**
     * @var string
     */
    protected $table = 'Messages';

    /**
     * @var string[]
     */
    protected $primaryKey = ['room_uuid'];

    /**
     * @var string[]
     */
    protected $compositeKey = ['room_uuid', 'chat_message_identifier'];

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string[]
     */
    protected $fillable = [
        'room_uuid',
        'chat_message_identifier',
        'body',
    ];
}
