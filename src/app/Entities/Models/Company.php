<?php

namespace App\Entities\Models;

use App\Entities\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Company
 * @package App\Entities\Models
 */
class Company extends Model
{
    use UuidTrait;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function organizations()
    {
        return $this->hasMany(Organization::class);
    }
}
