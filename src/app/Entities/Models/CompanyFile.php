<?php

namespace App\Entities\Models;

use App\Entities\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * Class CompanyFile
 * @package App\Entities\Models
 */
class CompanyFile extends Model
{
    use UuidTrait;

    /**
     * @var string[]
     */
    protected $fillable = [
        'company_id',
        'type',
        'url',
    ];
}
