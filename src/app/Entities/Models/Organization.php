<?php

namespace App\Entities\Models;

use App\Entities\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Organization
 * @package App\Entities\Models
 */
class Organization extends Model
{
    use UuidTrait;

    /**
     * @var string[]
     */
    protected $fillable = [
        'company_id',
        'category_id',
        'name',
        'description',
        'status',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(OrganizationCategory::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function childs()
    {
        return $this->hasManyThrough(
            Organization::class,
            OrganizationClosure::class,
            'parent_id',
            'id',
            'id',
            'child_id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function image()
    {
        return $this->hasOne(OrganizationImage::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function parents()
    {
        return $this->hasManyThrough(
            Organization::class,
            OrganizationClosure::class,
            'child_id',
            'id',
            'id',
            'parent_id'
        );
    }
}
