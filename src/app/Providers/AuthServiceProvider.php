<?php

namespace App\Providers;

use Illuminate\Foundation\Application;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        $this->app['auth']->provider(
            'custom_user_eloquent',
            function (Application $app, array $config) {
                return new CustomEloquentUserProvider(
                    $app['hash'],
                    $config['model']
                );
            }
        );
    }
}
