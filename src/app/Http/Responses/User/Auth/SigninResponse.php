<?php

namespace App\Http\Responses\User\Auth;

/**
 * Class SigninResponse
 * @package App\Http\Responses\User\Auth
 */
class SigninResponse
{
    /**
     * @param string $token
     * @param string $tokenType
     * @param int $expiresIn
     * @return array
     */
    public static function body(string $token, string $tokenType, int $expiresIn): array
    {
        return [
            'access_token' => $token,
            'token_type' => $tokenType,
            'expires_in' => $expiresIn,
        ];
    }
}
