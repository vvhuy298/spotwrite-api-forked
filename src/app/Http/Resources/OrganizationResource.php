<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class OrganizationResource
 * @package App\Http\Resources
 */
class OrganizationResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'uuid' => $this->uuid,
            'category_uuid' => $this->category->uuid ?? null,
            'category' => $this->category->name ?? null,
            'name' => $this->name,
            'description' => $this->description,
            'status' => $this->status,
            'file_uuid' => $this->image->uuid ?? null,
        ];
    }
}
