<?php

namespace App\Http\Controllers\Company;

use App\Http\Controllers\Controller;
use App\Http\Responses\Company\Auth\SigninResponse;
use Auth;
use Illuminate\Http\Response;

/**
 * Class AuthController
 * @package App\Http\Controllers\Company
 */
class AuthController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Auth\Guard|\Illuminate\Contracts\Auth\StatefulGuard
     */
    public function guard()
    {
        return Auth::guard('company');
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function signin()
    {
        $credentials = request(['email', 'password']);
        $token = $this->guard()->attempt($credentials);
        if (!$token) {
            return response()->json([], Response::HTTP_UNAUTHORIZED);
        }
        $tokenType = 'bearer';
        $ttl = $this->guard()->factory()->getTTL() * 60;

        return response()->json(SigninResponse::body($token, $tokenType, $ttl));
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function signout()
    {
        $this->guard()->logout();

        return response()->json();
    }
}
