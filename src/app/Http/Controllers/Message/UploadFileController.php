<?php

namespace App\Http\Controllers\Message;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use App\Http\Requests\User\Message\UploadFileRequest;
use Illuminate\Http\JsonResponse;

/**
 * Class MessageController
 * @package App\Http\Controllers
 */
class UploadFileController extends Controller
{
    /***
     * @param UploadFileRequest $request
     * @return JsonResponse
     */
    public function store(UploadFileRequest $request): JsonResponse
    {
        $file = $request->file('file');

        // 会社ID
        $companyId = auth()->user()->company->uuid;

        return response()->json([
            'file_url' => $this->createFileUrl($companyId, $file),
        ]);
    }

    /***
     * @param string $companyId
     * @param $file
     * @return string
     */
    private function createFileUrl(string $companyId, $file): string
    {
        $fileDir = $companyId . '/file';
        $filePath = Storage::disk()->putFile($fileDir, $file, ['public']);

        return Storage::disk()->url((string) $filePath);
    }
}
