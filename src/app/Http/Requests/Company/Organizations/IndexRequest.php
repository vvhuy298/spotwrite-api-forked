<?php

namespace App\Http\Requests\Company\Organizations;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class IndexRequest
 * @package App\Http\Requests\Company\Organizations
 */
class IndexRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_name' => ['string'],
            'main_organization' => ['string'],
            'employment_status' => ['string'],
            'employment_position' => ['string'],
            'permission' => ['string'],
            'status' => ['string'],
            'latest_access_date' => ['integer'],
        ];
    }
}
