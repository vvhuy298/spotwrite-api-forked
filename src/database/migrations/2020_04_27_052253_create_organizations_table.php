<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrganizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organizations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('uuid')->unique()->comment('UUID');
            $table->unsignedBigInteger('company_id')->comment('企業ID');
            $table->unsignedBigInteger('category_id')->comment('カテゴリID');
            $table->string('name')->comment('組織名');
            $table->string('description')->comment('組織説明文');
            $table->string('status')->comment('ステータス'); // TODO: 具体的な値は仕様が決まってから検討する
            $table->timestamps();

            $table->foreign('company_id')->references('id')->on('companies');
            $table->foreign('category_id')->references('id')->on('organization_categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organizations');
    }
}
