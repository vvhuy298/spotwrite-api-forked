<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Entities\Models\AccountHolder;
use Faker\Generator as Faker;

$factory->define(AccountHolder::class, function (Faker $faker) {
    return [
        'email' => $faker->unique()->safeEmail,
        'password' =>  $faker->password,
        'name' => $faker->name,
    ];
});
