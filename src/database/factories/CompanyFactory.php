<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Entities\Models\Company;
use Faker\Generator as Faker;

$factory->define(Company::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'address' => $faker->address,
        'tel' => $faker->phoneNumber,

        // TODO: size に何が入るか確認してからこっちも修正する
        'size' => '100人規模',
    ];
});
