<?php

namespace develop;

use Carbon\Carbon;
use DB;
use Illuminate\Database\Seeder;
use Str;

/**
 * Class EmploymentsTableSeeder
 * @package develop
 */
class EmploymentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->insertEmploymentPositions();
        $this->insertEmploymentStatuses();
    }

    /**
     *
     */
    private function insertEmploymentPositions()
    {
        DB::table('employment_positions')->insert([
            [
                'id' => 1,
                'uuid' => Str::uuid()->toString(),
                'company_id' => 1,
                'name' => '役職01',
                'is_activate' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
        ]);
    }

    /**
     *
     */
    private function insertEmploymentStatuses()
    {
        DB::table('employment_statuses')->insert([
            [
                'id' => 1,
                'uuid' => Str::uuid()->toString(),
                'company_id' => 1,
                'name' => '雇用形態01',
                'display_name' => '雇用形態01の表示名',
                'is_activate' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
        ]);
    }
}
