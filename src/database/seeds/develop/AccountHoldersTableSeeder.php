<?php

namespace develop;

use Carbon\Carbon;
use DB;
use Illuminate\Database\Seeder;
use Str;

class AccountHoldersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('account_holders')->insert([
            [
                'id' => 1,
                'uuid' => Str::uuid()->toString(),
                'company_id' => 1,
                'email' => 'account01@example.com',
                'password' => bcrypt('password'),
                'name' => 'アカウントホルダー01',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
        ]);

    }
}
