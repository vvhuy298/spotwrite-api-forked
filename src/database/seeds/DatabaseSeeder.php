<?php

use Illuminate\Database\Seeder;

/**
 * Class DatabaseSeeder
 */
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        if (App::environment() === 'production') {
            //
        } else {
            $this->call([
                develop\CompaniesTableSeeder::class,
                develop\AccountHoldersTableSeeder::class,
                develop\UsersTableSeeder::class,
                develop\EmploymentsTableSeeder::class,
                develop\OrganizationsTableSeeder::class,
            ]);
        }
    }
}
