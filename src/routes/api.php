<?php

Route::group(['prefix' => 'company', 'as' => 'company.', 'namespace' => 'Company'], function () {
    Route::post('signin', 'AuthController@signin')->name('signin');

    Route::group(['middleware' => 'auth:company'], function () {
        Route::post('signout', 'AuthController@signout')->name('signout');

        Route::group(['prefix' => 'organizations', 'as' => 'organizations.'], function () {
            Route::get('/', 'OrganizationsController@index')->name('index');
            Route::get('{organization}', 'OrganizationsController@show')->name('show');
            Route::post('{organization?}', 'OrganizationsController@create')->name('create');
        });
    });
});

Route::group(['namespace' => 'User'], function () {
    Route::post('signin', 'AuthController@signin')->name('signin');

    Route::group(['middleware' => 'auth:user'], function () {
        Route::post('signout', 'AuthController@signout')->name('signout');
    });
});

Route::group(['prefix' => 'message', 'as' => 'message.', 'namespace' => 'Message', 'middleware' => 'auth:user'], function () {
    Route::get('/', 'MessageController@getAllMessages');
    Route::post('/text', 'MessageController@storeText')->name('store.text');
    Route::post('/roger', 'MessageController@storeRoger')->name('store.roger');
    Route::post('/room', 'RoomController@store')->name('room.store');
    Route::get('/rooms', 'RoomController@index')->name('room.index');
    Route::post('/roger', 'MessageController@storeRoger')->name('store.roger');
    Route::post('/uploadImage', 'UploadImageController@store')->name('store.uploadImage');
    Route::post('/images', 'MessageController@storeImages')->name('store.images');
    Route::post('/uploadFile', 'UploadFileController@store')->name('store.uploadFile');
    Route::post('/files', 'MessageController@storeFiles')->name('store.files');
});
