<?php

namespace Tests\Feature\Http\User;

use App\Entities\Models\User;
use App\Entities\Models\UserAuthentication;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Tests\TestCase;

/**
 * Class AuthTest
 * @package Tests\Feature\Http\User
 */
class AuthTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @var User
     */
    private User $user;

    /**
     * @noinspection NonAsciiCharacters
     * @test
     */
    public function ログインするとJWTトークンが返る(): void
    {
        $request = [
            'email' => $this->user->authentication->email,
            'password' => 'password',
        ];
        $response = $this->postJson(route('signin'), $request);

        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson([
            'token_type' => 'bearer',
            'expires_in' => 3600,
        ]);
        $response->assertJsonStructure([
            'access_token',
            'token_type',
            'expires_in',
        ]);
    }

    /**
     * @noinspection NonAsciiCharacters
     * @test
     */
    public function ログインに失敗すると401が返る(): void
    {
        // emailが未指定
        $request = [
            'password' => 'password',
        ];
        $response = $this->postJson(route('signin'), $request);
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);

        // passwordが未指定
        $request = [
            'email' => $this->user->email,
        ];
        $response = $this->postJson(route('signin'), $request);
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);

        // emailが未登録
        $request = [
            'email' => 'test@test.com',
            'password' => 'password',
        ];
        $response = $this->postJson(route('signin'), $request);
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);

        // パスワードが誤っている
        $request = [
            'email' => $this->user->email,
            'password' => 'incorrect-password',
        ];
        $response = $this->postJson(route('signin'), $request);
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    /**
     * @noinspection NonAsciiCharacters
     * @test
     */
    public function ログアウトできる(): void
    {
        $request = [
            'email' => $this->user->authentication->email,
            'password' => 'password',
        ];
        $this->postJson(route('signin'), $request);
        $response = $this->postJson(route('signout'));
        $response->assertStatus(Response::HTTP_OK);
    }

    /**
     * @noinspection NonAsciiCharacters
     * @test
     */
    public function 未ログイン状態でログアウトすると401エラーになる(): void
    {
        $response = $this->postJson(route('signout'));
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    /**
     *
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->user = factory(User::class)->create();
        $this->user->authentication()->save(factory(UserAuthentication::class)->make([
            'password' => 'password',
        ]));
    }
}
